package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);
        multiplicationTable(3);
        System.out.println(crossSum(123));
    }

    public static void multiplesOfSevenUpTo(int n) {
        int i = 0;
        while (i<=n){
            if (i>0 && i== Math.round(i) && i%7==0){
                System.out.println(i);
            }
            i++;
        }
        //throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public static void multiplicationTable(int n) {
        String string = "";
        for (int i=1; i<=n; i++){
            for (int j=1; j<=n; j++){
                string += (j*i + " ");
            }
            System.out.println(i+": " +string);
            string = "";
        }
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static int crossSum(int num) {
        int numResult = 0;
        String numString = String.valueOf(num);

        for (int i = 0; i< numString.length(); i++){
            char numberInString = numString.charAt(i);
            int numberInInt = Character.getNumericValue(numberInString);
            numResult += numberInInt;
        }
        return numResult;

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}