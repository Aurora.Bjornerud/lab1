package INF101.lab1.INF100labs;

//import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
//import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Integer> multipliedList1 = multipliedWithTwo(list1);
        System.out.println(multipliedList1);

        ArrayList<Integer> removed3List = removeThrees(list1);
        System.out.println(removed3List);

        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2));
        ArrayList<Integer> uniqueList = uniqueValues(list2);
        System.out.println(uniqueList);

        ArrayList<Integer> a = new ArrayList<>(Arrays.asList(1, 2, 3));
        ArrayList<Integer> b = new ArrayList<>(Arrays.asList(4, 2, -3));
        addList(a, b);

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        
        ArrayList<Integer> resultList = new ArrayList<>();
        for (int num: list){
            int numNew = num*2;
            resultList.add(numNew);
        }
        return resultList;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        list.removeAll(Arrays.asList(3));
        return list;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> unique = new ArrayList<>();
        for (int num: list){
            if(!(unique.contains(num))){
                unique.add(num);
            }
        }
        return unique;
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i<a.size();i++){
            int aNumber = a.get(i);
            int bNumber = b.get(i);
            int newA = aNumber+bNumber;
            a.set(i, newA);
        }
        System.out.println(a);
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}