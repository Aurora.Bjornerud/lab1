package INF101.lab1.INF100labs;

import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/1/
 */
public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        task1();
        task2();
        
    }

    public static void task1() {
        //String hello = "Hei, det er meg, datamaskinen.\n" + "Hyggelig å se deg her.\n" + "Lykke til med INF101! \n";
        System.out.println("Hei, det er meg, datamaskinen.");
        System.out.println("Hyggelig å se deg her.");
        System.out.println("Lykke til med INF101!");
        
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line
        String navn = readInput("Hva er ditt navn?");
        String adresse = readInput("Hva er din adresse?");
        String postnummer = readInput("Hva er ditt postnummer og poststed?");

        //String person = navn + "s adresse er: \n\n"+ navn + "\n" + adresse + "\n" + postnummer;
        String person = String.format("%1$ss adresse er: \n\n%1$s\n%2$s\n%3$s", navn, adresse, postnummer);
        System.out.println(person); 
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }


}
