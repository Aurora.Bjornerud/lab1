package INF101.lab1.INF100labs;

import java.util.ArrayList;
//import java.util.Comparator;
import java.util.List;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("apple", "carrot", "ananas");
        isLeapYear(2022);
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        List<String> myList = new ArrayList<>();
        
        myList.add(word1);
        myList.add(word2);
        myList.add(word3);

        List<String> longest = new ArrayList<>();
        longest.add("");
        
        for (String word: myList){
            int length = word.length();

            int lengthLongest = (longest.get(0)).length();

            if (length >= lengthLongest){
                if (length>lengthLongest){
                    longest.remove(0);
                }
                longest.add(word);
            }
        }

        for (String result: longest){
            System.out.println(result);
        }

        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isLeapYear(int year) {
        boolean test1 = year % 4 == 0;
        boolean test2 = year % 100 == 0;
        boolean test3 = year % 400 == 0;
        return ((test1&&!test2 ) || (test2&&test3));
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean isEvenPositiveInt(int num) {
        boolean test1 = (num== Math.round(num));
        boolean test2 = (num>0) && (num%2==0);
        return (test1 && test2);
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}

