package INF101.lab1.INF100labs;

//import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid = new ArrayList<>();
        grid.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid.add(new ArrayList<>(Arrays.asList(31, 32, 33)));
        removeRow(grid, 2);

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid2.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid2.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        System.out.println(allRowsAndColsAreEqualSum(grid2));
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
        System.out.println(grid);
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int oldSumRow = 0;                                              // Sum of first row
        ArrayList<Integer> colSum = new ArrayList<>();                  // Sum of the collumns
        boolean result = true;
        for (int i = 0; i<grid.size() ;i++){
            int sumRow = 0;
            for (int j = 0; j<(grid.get(i)).size(); j++){
                sumRow += (grid.get(i)).get(j);                         // Adding the values of a row together
                if (i==0){                                              // If it is the first row
                    colSum.add((grid.get(i)).get(j));                   // Add the values of the first row to a list
                }
                else{
                    int newValue = 0;
                    newValue += (colSum.get(j) + (grid.get(i)).get(j)); // Adding the numbers in a column
                    colSum.set(j,newValue);                             // Updating the set to the combined value of a column so far
                }
            }
            if(i==0){                                                   // If it is the first row
                oldSumRow = sumRow;                                     // The oldSum is the sum of the first row
            }
            result = (oldSumRow - sumRow==0);                           // True if the sum of row matches the first row
            if (result == false){                                       
                return false;                                           // Return false and stop the loop if a false is found
            }
        } 
        int oldColSum = colSum.get(0);                                  // oldColSum is the sum of the first column
        for (int sum: colSum){                                          // Looping through the sums of colmns
            result = (oldColSum == sum);
            if (result == false){
                return false;                                           // Return false and stop the loop if a false is found
            }
        }
        return result;                                                  // Returns true if no false was found
        //throw new UnsupportedOperationException("Not implemented yet.");
    }

}